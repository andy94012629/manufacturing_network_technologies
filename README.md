Manufacturing_Network_Technologies
# 簡介
本專案為2020秋季 國立中央大學  智慧製造聯網技術 期末專題

# 架構
包含 \
以**C語言**編撰的 \
I2Chandler 、 arduuino 模組

以**python**編撰的 \
mqtt_2_i2c 模組

node-red模組\
sqlite模組

# I2Chandler

本模組主要給專題中終端微控制器使用 \
並搭配mqtt_2_i2c模組 達成I2C和MQTT轉換 \
平台為AVR atmega128 及 atmega328

# arduuino

本模組為RF reader終端所使用到的程式 \
會定時且重複輪巡RF reader模組 \
RF reader使用 MFRC522 \
平台為atmega328 

# mqtt_2_i2c

本模組為實現I2C和MQTT轉換的主幹 \
平台為raspberry pi 4 model B

# node-red

本模組存放裝置聯網後，轉換成資訊展示在網站圖表上 \
或是存入database中

# sqlite

此模組存放database以及網頁爬蟲程式 \
爬蟲目標網站為CWB(中央氣象局) \
欲取得目標為中央大學氣候資訊
