#define F_CPU 16000000UL
#define DEFAULTUARTBAUD 9600
#include "mylib.h"
#include "math.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>


int stdio_putchar(char c, FILE *stream);
int stdio_getchar(FILE *stream);
void UNO_stdio_set(void);

static FILE STDIO_BUFFER = FDEV_SETUP_STREAM(stdio_putchar, stdio_getchar, _FDEV_SETUP_RW);

// Based on ATMega328 datasheet p.184, Sending Frames with 5 to 8 Data Bit
int stdio_putchar(char c, FILE *stream) 
{
	// 超級終端機中使用CRLF('\n'+'\r')作為完整換行
	// 當使用者使用'\n'換行時需補送'\r'
    if (c == '\n') 
        stdio_putchar('\r',stream); 
	
	// 等待暫存器清空, UART0傳送資料完成時UCSR0A中UDRE0旗標會自動設為0
	// 確認暫存器空才能放入新資料 
	// ATMega328 datasheet p.184, UCSRnA
    while(!(UCSR0A&(1<<UDRE0))); 

	// 將待傳送資料放入UART0資料暫存器
    UDR0 = c;
	
    return 0; 
}

// Based on ATMega328 datasheet p.187, Receiving Frames with 5 to 8 Data Bits
int stdio_getchar(FILE *stream)
{
	int UDR_Buff;
	
	// 等待資料接收, UART0接收到資料時UCSR0A中RXC0旗標會自動設為0
	// 確認收到新資料才能從暫存器取值
	// ATMega328 datasheet p.187, UCSRnA
    while(!(UCSR0A&(1<<RXC0)));
	
	// 將UART0資料暫存器中的資料取出
	UDR_Buff = UDR0;
	
	// 將超級終端機中輸入的資料傳送回去, 否則使用scanf時被傳送到AVR的資料不會顯示在超級終端機畫面中
	stdio_putchar(UDR_Buff,stream);

	return UDR_Buff;
}

void UNO_stdio_set(void)
{
	unsigned int ubrr_data;

	// Asynchronous Normal Mode 
	ubrr_data = F_CPU/16/DEFAULTUARTBAUD-1;
	UBRR0H = (unsigned char)(ubrr_data>>8);
	UBRR0L = (unsigned char)ubrr_data;
	
	// UCSR0B
	// Rx Enable, Tx Enable
	UCSR0B |= (1<<RXEN0) | (1<<TXEN0);
	
	// UCSRnC
	// Character Size : 8-bit
	UCSR0C |= (1<<UCSZ01) | (1<<UCSZ00);

	// 指定printf、scanf使用的暫存區
	stdout = &STDIO_BUFFER;
	stdin = &STDIO_BUFFER;
}

void SPI_MasterInit(void)
{
	/* Set MOSI(PB3), SCK(PB5) and SS(PB2) output*/
	DDRB |= (1<<PB2) | (1<<PB3) | (1<<PB5); 
	PORTD |= (1<<PB2); // SS high
	// MISO(PB4) as input
	DDRB &= ~(1<<PB4);
	// SPI mode = 0
	SPCR &= ~(1<<CPOL); // 上升緣讀寫
	SPCR &= ~(1<<CPHA); // SCLK奇次變化時讀寫
	// SCLKmin=100ns=0.1us => Freq_max=1/0.1us=10MHz
	// we chose fosc = 1.10592MHz *4/ 2 =5.5296MHz
	SPSR |= (1<<SPI2X); //f/2
	SPCR &= ~((1<<SPR1)|(1<<SPR0));
	// 高位元先送
	SPCR &= ~(1<<DORD);
	// M128 is master
	SPCR |= (1<<MSTR);
	// SPI enable
	SPCR |= (1<<SPE);
}


char SPI_Transmit(char cData)
{
	/* Start transmission */
	SPDR = cData;
	while(!(SPSR & (1<<SPIF)));
	return SPDR;
}
