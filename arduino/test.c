#define F_CPU 16000000UL
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdbool.h>
#include "mylib.h"
#include "RFID.h"

int main (void){
    UNO_stdio_set();
    SPI_Set();
    RFID_Set();

	printf("--------------------START------------------------\n");
    while(1){
		while(!IsNewCardPresent());		
		while(!ReadCardSerial());
		printf("A new card detected\n");
        for(uint8_t i = 0; i < data.size; i++){
			printf("data[%d] = %x\n",i,data.uidByte[i]);
		}
		PICC_HaltA();
		PCD_StopCrypto1();
    }
}