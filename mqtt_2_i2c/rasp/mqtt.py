import time
import paho.mqtt.client as mqtt
from rasp.i2c import i2c_handler
import struct
import subprocess

I2C = i2c_handler()

def on_connect(client, userdata, flags, rc):
    print("Connected with result code" + str(rc))
    client.subscribe("Temp")
    client.subscribe("Pump")
    client.subscribe("RFID")

def on_message(client, userdata, msg):
    if (msg.topic == 'Temp'):
        print('Temp')
        Temp = msg.payload.decode('utf-8')
        print(Temp)
        I2C.send_tmp_order(0x38, int(msg.payload))
    elif (msg.topic == 'Pump'):
        print('Pump')
        print(msg.payload.decode('utf-8'))
        if (I2C.send_pump_order(0x28, int(msg.payload))):
            client.publish("CHK_Pump",time.time())


def iot_publish(client):
    #try:
    #    data = I2C.get_tmp_data(0x38)
    #    time.sleep(1)
    #except IOError:
    #    subprocess.call(['i2cdetect', '-y', '1'])
    #    time.sleep(1)
    #    data = I2C.get_tmp_data(0x38)
    #print(I2C.tmp)
    #client.publish("CHK_Temp",I2C.tmp)
    time.sleep(1)
    if (I2C.is_pump_switched(0x28)):
        client.publish("CHK_Pump",time.time())

    #time.sleep(1)
    #client.publish("CHK_RFID",)

def mqtt_setting(client):
    client.on_message=on_message
    client.on_connect = on_connect


def mqtt_init():
    return mqtt.Client()

def run():
    
    client = mqtt.Client()

    client.on_message=on_message

    client.on_connect = on_connect

    client.connect("192.168.1.110",port=1883,keepalive=60)

    client.subscribe("Temp")
    client.subscribe("Pump")
    client.subscribe("RFID")

    
    client.loop_forever()
    #client.loop_start()
    #client.publish("CHK_temp",20.22)
    #time.sleep(10)

if __name__ == '__main__':
    run()