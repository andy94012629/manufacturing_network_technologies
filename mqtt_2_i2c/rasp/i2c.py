from smbus2 import SMBus
import struct
from time import sleep

class i2c_handler():
    def __init__(self):
        self.i2c = SMBus(1)
        sleep(2)
        self.tmp = 0
        self.pump_state = 0
        self.heating_state = 0

    def send_tmp_order(self, addr, data):
        self.tmp = data
        self.i2c.write_byte_data(addr, 0x02, data)

    def get_tmp_data(self, addr):
        data = self.i2c.read_i2c_block_data(addr, 0x03, 1)
        self.tmp = data[0]
        return self.tmp

    def get_rfid_data(self, addr):
        return self.i2c.read_i2c_block_data(addr, 0x03, 1)

    def send_pump_order(self, addr, data):
        tmp = self.i2c.read_i2c_block_data(addr, 0x03, 1)
        self.pump_state = tmp[0]
        sleep(2)
        if (self.pump_state != data):
            self.i2c.write_byte_data(addr, 0x02, data)
            self.pump_state = data
            return 1
        else:
            return 0

    def is_pump_switched(self, addr):
        tmp =  self.i2c.read_i2c_block_data(addr, 0x03, 1)
        if (self.pump_state != tmp[0]):
            self.pump_state = tmp[0]
            return 1
        else:
            return 0


def run():
    data_float = 123.45
    data = struct.pack('<f', data_float)
    #print(data_float)
    print(len(data))
    print(type(data))
    print(data)

if __name__ == "__main__":
    run()