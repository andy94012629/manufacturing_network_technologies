# coding: utf-8
from bs4 import BeautifulSoup
from selenium import webdriver
import sqlite3

def web():
    #開啟sqlite資料庫
    db = sqlite3.connect('./finalproject.db') 
    cur = db.cursor()
    #查看已擁有資料並用時間降階排列
    database = cur.execute("SELECT * FROM NCU ORDER BY time DESC")
    times = list(row[0] for row in database)
    # print(times)

    #全臺測站分區 - 中央大學測站列表
    url = 'https://www.cwb.gov.tw/V8/C/W/OBS_Station.html?ID=A0C41'

    #啟動模擬瀏覽器
    driver = webdriver.Chrome(executable_path = './chromedriver.exe')

    #取得網頁代馬
    driver.get(url)

    #指定 lxml 作為解析器
    soup = BeautifulSoup(driver.page_source, features='lxml')

    #<tbody id='stations'>
    tbody = soup.find('tbody',{'id':'obstime'})

    #<tbody>内所有<tr>標籤
    trs = tbody.find_all('tr')

    #對list中的每一項 <tr>
    for tr in trs:


        thdate = tr.find('th',{'headers':'time'})
        date = thdate.next_element
        

        brtime = thdate.find(class_ = 'visible-md')
        time = brtime.next_element
        data = date + time

        tdtemp = tr.find('span',{'class':'tem-C is-active'})
        temp = tdtemp.next_element
        #print(data ,temp)

        tdweather = tr.find('img')
        weather = tdweather['title']
        
        if data not in times:
            values = [data,temp,weather]
            # print(values)
            cur.execute('INSERT INTO NCU (time, temperature, weather) VALUES (?, ?, ?)', values)
        db.commit()

    #與sqlite中斷
    db.close()

    #關閉模擬瀏覽器       
    driver.quit()

if __name__ == '__main__':
    web()