import sys
sys.path.insert(0, "..")
import time
from NCU import web

from opcua import ua, Server
import sqlite3

if __name__ == "__main__":
   

    # setup our server
    server = Server()
    server.set_endpoint("opc.tcp://127.0.0.1:4840/freeopcua/server/")

    # setup our own namespace, not really necessary but should as spec
    uri = "http://examples.freeopcua.github.io"
    idx = server.register_namespace(uri)

    # get Objects node, this is where we should put our nodes
    objects = server.get_objects_node()

    # populating our address space
    city = objects.add_object(idx, "Taoyuan")
    location = city.add_object(idx, "NCU")
    NCUtime = location.add_variable(idx, "times", "")
    NCUtemp = location.add_variable(idx, "temp", 0)
    NCUweather = location.add_variable(idx, "weather", "")
    
    
    #NCUtemp.set_writable()    # Set MyVariable to be writable by clients

    # starting!
    server.start()
    
    try:
        while True:
            #開啟sqlite資料庫
            db = sqlite3.connect('./finalproject.db') 
            cur = db.cursor()
            #查看已擁有資料並用時間降階排列
            database = cur.execute("SELECT * FROM NCU ORDER BY time DESC")
            data = database.fetchone()
            #與sqlite中斷
            db.close()
            NCUtime.set_value(data[0])
            NCUtemp.set_value(data[1])
            NCUweather.set_value(data[2])
            time.sleep(600)
            web()
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()