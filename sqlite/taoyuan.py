# coding: utf-8
from bs4 import BeautifulSoup
from selenium import webdriver
import sqlite3

#開啟sqlite資料庫
db = sqlite3.connect('./finalproject.db')
cur = db.cursor()

#查看已擁有資料
database = cur.execute("SELECT * FROM Taoyuan")
#取出location資料
locations = list(row[0] for row in database)

#全臺測站分區 - 桃園市測站列表
url = 'https://www.cwb.gov.tw/V8/C/W/OBS_County.html?ID=68'

#啟動模擬瀏覽器
driver = webdriver.Chrome()

#取得網頁代馬
driver.get(url)

#指定 lxml 作為解析器
soup = BeautifulSoup(driver.page_source, features='lxml')

#<tbody id='stations'>
tbody = soup.find('tbody',{'id':'stations'})

#<tbody>内所有<tr>標籤
trs = tbody.find_all('tr')

#對list中的每一項 <tr>
for tr in trs:

    #<tr>內的<th> <a>標籤
    th = tr.th.a
    
    #取得下個標籤內的文字
    name = th.next_element

    #<tr>內的<td>標籤
    tdtime = tr.find('td',{'headers':'OBS_Time'})
    time = tdtime.next_element

    #<tr>內的<td>標籤
    tdtemp = tr.find('td',{'headers':'temp'})
    temp = tdtemp.next_element
    print(name, time, temp)

    #判斷資料是否已存取過
    if name in locations:
        values = [time,temp,name]
        #更新資料
        cur.execute('UPDATE Taoyuan SET time=?, temperature =? WHERE location=?', values)
    else:
        values = [name,time,temp]
        #插入新資料
        cur.execute('INSERT INTO Taoyuan (location, time, temperature) VALUES (?, ?, ?)', values)
    db.commit()

#關閉模擬瀏覽器       
driver.quit()

#與sqlite中斷
db.close()