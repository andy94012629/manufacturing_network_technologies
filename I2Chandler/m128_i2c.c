/**
 * @file m128_i2c_handler.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2020.12.17
 * @brief 
 * 
 */
#include <avr/io.h>
#include <stdio.h>
#include "hal_i2c_handler.h"

static void I2C_init(uint8_t addr);
static uint8_t I2C_read_byte(void);
static void I2C_write_byte(uint8_t data);
static void I2C_write_final_byte(uint8_t data);
static void I2C_ackcom_ctrl(uint8_t ack);
static uint8_t I2C_status(void);
static void I2C_clear_interrupt_flag(void);

_i2c_handler_t hal_i2c = {
	.init = I2C_init,
	.read_byte = I2C_read_byte,
	.write_byte = I2C_write_byte,
	.write_final_byte = I2C_write_final_byte,
	.ackcom_ctrl = I2C_ackcom_ctrl,
	.status_chk = I2C_status,
	.clear_i2c_int_flag = I2C_clear_interrupt_flag};

static void I2C_init(uint8_t addr)
{
	const uint16_t baud = 38400;
	TWBR = ((F_CPU / baud) - 16) / 2;
	TWCR = 0;
	TWCR |= 1 << TWIE; //TWI INTERRUPT ENABLE
	TWCR |= 1 << TWEN; //TWI EANBLE
	TWCR |= 1 << TWEA;
	TWAR = addr << 1;
	printf("Initial done ! address = %d\n",addr);
}

static uint8_t I2C_read_byte(void)
{
	while (!(TWCR & 0x80));
	return TWDR;
}

static void I2C_write_final_byte(uint8_t data)
{
	while (TWSR != 0xA8 && TWSR != 0xB0);
	TWDR = data;
	TWCR &= ~(1 << TWEA);
	return;
}

static void I2C_write_byte(uint8_t data)
{
	while (TWSR != 0xA8 && TWSR != 0xB0);
	TWDR = data;
	TWCR |= (1 << TWEA);
	return;
}

static void I2C_ackcom_ctrl(uint8_t ack){
    if (ack == USE_ACK){      
		TWCR |= 1 << TWEA;
	}                     
	else if (ack == USE_NACK){ 
		TWCR &= ~(1 << TWEA); 
	}
    return;
}

static void I2C_clear_interrupt_flag(void){
	TWCR |= 1 << TWINT;
	return;
}

static uint8_t I2C_status(void){
	uint8_t stat = 0;
	switch (TWSR)
	{
		/**
		 * @brief  slave recieve mode
		 * 
		 */
		case 0x60:
		case 0x68:{
			stat = ACK_SLA_W;
			break;
		}
		case 0x80:{
			stat = MASTER_REC_SEND_ACK;
			break;
		}
		case 0x88:{
			stat = MASTER_REC_SEND_NACK;
			break;
		}
        case 0x70:
		case 0x78:{
			stat = ACK_GENERAL_CALL;
			break;
		}
		case 0x90:{
			stat = GENRAL_REC_SEND_ACK;
			break;
		}
		case 0x98:{
			stat = GENRAL_REC_SEND_NACK;
			break;
		}
		case 0xA0:{
			stat = MASTER_GENERAL_STOP;
			break;
		}
		/**
		 * @brief  slave transmit mode 
		 * 
		 */
        case 0xA8:
		case 0xB0:{
			stat = ACK_SLA_R;
            break;
		}
		case 0xC8:
		case 0xC0:{
			stat = DATA_TRANS_FINISHED;
			break;
		}
		default:{
		    stat = STA_ERROR;
			break;
		}
	}
	return stat;
}
