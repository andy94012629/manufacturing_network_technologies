/**
 * @file hal_i2c_handler.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2020.12.17
 * @brief 
 * 
 */

#ifndef HAL_I2C_HANDLER_H
#define HAL_I2C_HANDLER_H

// #if defined __atmega128__
//     #define CPU = 11059200UL
// #elif defined __atmega328p__
//     #define CPU = 16000000UL
// #endif

#define USE_ACK  1
#define USE_NACK 0

#define ACK_SLA_W                  0x00
#define ACK_GENERAL_CALL           0x01
#define GENRAL_REC_SEND_ACK        0x02
#define MASTER_REC_SEND_ACK        0x03
#define GENRAL_REC_SEND_NACK       0x04
#define MASTER_REC_SEND_NACK       0x05
#define MASTER_GENERAL_STOP        0x06

#define ACK_SLA_R                  0x10
#define TRANS_NEXT_DATA            0x11
#define DATA_TRANS_FINISHED        0x12

#define STA_ERROR                  0xFF


#include <stdint.h>
#include <string.h>

#define VAR_TYPE_UI8         0
#define VAR_TYPE_I8          1
#define VAR_TYPE_UI16        2
#define VAR_TYPE_I16         3
#define VAR_TYPE_UI32        4
#define VAR_TYPE_I32         5
#define VAR_TYPE_UI64        6
#define VAR_TYPE_I64         7
#define VAR_TYPE_FLOAT       8
#define VAR_TYPE_DOUBLE      9

typedef struct _i2c_package{
    void **var_p[4];
    uint8_t var_type[4];
    uint8_t tmp[8];
    uint8_t decode_stat;
    uint8_t cmd;
    uint8_t trans_data_total;
    uint8_t index;
    uint8_t total;
}_i2c_package_t;

typedef struct _i2c_handler{
    void (*init)(uint8_t);
    uint8_t (*read_byte)(void);
    void (*write_byte)(uint8_t);
    void (*write_final_byte)(uint8_t);
    void (*ackcom_ctrl)(uint8_t);
    uint8_t (*status_chk)(void);
    void (*clear_i2c_int_flag)(void);
}_i2c_handler_t;

_i2c_package_t *ptr;

void write_byte(uint8_t);

uint8_t read_byte(void);

void init(uint8_t);

void decoder(_i2c_package_t *);

uint8_t cmd_translate(uint8_t);

_i2c_package_t *init_i2c_package_t(void);

uint8_t reg_var_4_i2c(void *, uint8_t, _i2c_package_t *);

#endif /* HAL_I2C_HANDLER_H */
