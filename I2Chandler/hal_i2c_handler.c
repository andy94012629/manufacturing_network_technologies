/**
 * @file hal_i2c_handler.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2020.12.18
 * @brief 
 * 
 */


#include "hal_i2c_handler.h"
#include <stdlib.h>
/**
 * @brief  Decoder state
 * 
 */
#define DECODER_ADDR                   0
#define DECODER_CMD                    1
#define DECODER_TRANS                  2

/**
 * @brief comand types
 * 
 */
//TODO :重新安排cmd 內所包含的資訊：要包含data type ,R/W
//=============char data=================
#define CMD_WR_CHAR_BYTE               0x12
#define CMD_RD_CHAR_BYTE               0x13
//=========unsigned char data============
#define CMD_WR_UI8_BYTE                0x02
#define CMD_RD_UI8_BYTE                0x03
//=============i16  data=================
#define CMD_WR_I16_BYTE                0x32
#define CMD_RD_I16_BYTE                0x33
//=============ui16 data=================
#define CMD_WR_UI16_BYTE               0x22
#define CMD_RD_UI16_BYTE               0x23
//=============i32 data=================
#define CMD_WR_I32_BYTE                0x52
#define CMD_RD_I32_BYTE                0x53
//==============ui32 data===============
#define CMD_WR_UI32_BYTE               0x42
#define CMD_RD_UI32_BYTE               0x43
//=============float data===============
#define CMD_WR_FLOAT_BYTE              0x82
#define CMD_RD_FLOAT_BYTE              0x83
//==============i64 data================
#define CMD_WR_I64_BYTE                0x72
#define CMD_RD_I64_BYTE                0x73
//==============ui64 data===============
#define CMD_WR_UI64_BYTE               0x62
#define CMD_RD_UI64_BYTE               0x63
//=============double data===============
#define CMD_WR_DOUBLE_BYTE             0x92
#define CMD_RD_DOUBLE_BYTE             0x93

extern _i2c_handler_t hal_i2c;

void init(uint8_t addr){
    return hal_i2c.init(addr);
}

void write_byte(uint8_t data){
    return hal_i2c.write_byte(data);
}

uint8_t read_byte(void){
    return hal_i2c.read_byte();
}

uint8_t cmd_translate(uint8_t cmd){
	switch (cmd & 0xf3){
		case CMD_WR_CHAR_BYTE:
		case CMD_RD_CHAR_BYTE:
		case CMD_WR_UI8_BYTE:
		case CMD_RD_UI8_BYTE:{
			return 1;
		}
		case CMD_WR_I16_BYTE:
		case CMD_RD_I16_BYTE:
		case CMD_WR_UI16_BYTE:
		case CMD_RD_UI16_BYTE:{
			return 2;
		}
		case CMD_WR_I32_BYTE:
		case CMD_RD_I32_BYTE:
		case CMD_WR_UI32_BYTE:
		case CMD_RD_UI32_BYTE:
		case CMD_WR_FLOAT_BYTE:
		case CMD_RD_FLOAT_BYTE:{
			return 4;
		}
		case CMD_WR_I64_BYTE:
		case CMD_RD_I64_BYTE:
		case CMD_WR_UI64_BYTE:
		case CMD_RD_UI64_BYTE:
		case CMD_WR_DOUBLE_BYTE:
		case CMD_RD_DOUBLE_BYTE:{
			return 8;
		}
		default:{
		    return 0;
		}
	}
}

void decoder(_i2c_package_t *ptr){
	static uint8_t cnt = 0;
    uint8_t i2c_stat = hal_i2c.status_chk();
	if (i2c_stat == DATA_TRANS_FINISHED || i2c_stat == MASTER_GENERAL_STOP){
		if (!(ptr->cmd & 0x01) && (ptr->index <= ptr->total)){
				switch (ptr->var_type[ptr->index])
				{
				case VAR_TYPE_UI8:{
					*(uint8_t *)(ptr->var_p[ptr->index]) = *(uint8_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_I8:{
					*(int8_t *)(ptr->var_p[ptr->index]) = *(int8_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_I16:{
					*(int16_t *)(ptr->var_p[ptr->index]) = *(int16_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_UI16:{
					*(uint16_t *)(ptr->var_p[ptr->index]) = *(uint16_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_I32:{
					*(int32_t *)(ptr->var_p[ptr->index]) = *(int32_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_UI32:{
					*(uint32_t *)(ptr->var_p[ptr->index]) = *(uint32_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_I64:{
					*(int64_t *)(ptr->var_p[ptr->index]) = *(int64_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_UI64:{
					*(uint64_t *)(ptr->var_p[ptr->index]) = *(uint64_t *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_FLOAT:{
					*(float *)(ptr->var_p[ptr->index]) = *(float *)(ptr->tmp);
					break;
				}
				case VAR_TYPE_DOUBLE:{
					*(double *)(ptr->var_p[ptr->index]) = *(double *)(ptr->tmp);
					break;
				}
				default:
					break;
				}
		}
		cnt = 0;
		hal_i2c.ackcom_ctrl(USE_ACK);
		hal_i2c.clear_i2c_int_flag();
		return;
	}
	else if (i2c_stat == ACK_SLA_W){
		ptr->decode_stat = DECODER_CMD;
		hal_i2c.ackcom_ctrl(USE_ACK);
		hal_i2c.clear_i2c_int_flag();
		return;
	}
	switch (ptr->decode_stat){
		case DECODER_CMD:{
			ptr->cmd = hal_i2c.read_byte();
			ptr->index = ((ptr->cmd)  & 0x0c) >> 2;
			ptr->decode_stat = DECODER_TRANS;
			ptr->trans_data_total = cmd_translate(ptr->cmd);
			hal_i2c.ackcom_ctrl(USE_ACK);
			break;
		}
		case DECODER_TRANS:{
			if (((ptr->cmd) & 0x01) && (ptr->index <= ptr->total)){
				if (ptr->trans_data_total - 1 == cnt){
					hal_i2c.write_final_byte(*(uint8_t *)((ptr->var_p[ptr->index]) + cnt));
				}
				else{
					hal_i2c.write_byte(*(uint8_t *)((ptr->var_p[ptr->index]) + cnt));
					cnt++;
				}
				break;
			}
			else{
				ptr->tmp[cnt] = hal_i2c.read_byte();
				cnt++;
				hal_i2c.ackcom_ctrl(USE_ACK);
				break;
			}
		}
		default:{
			break;
		}
	}
	hal_i2c.clear_i2c_int_flag();
	return;
}

_i2c_package_t *init_i2c_package_t(void){
	_i2c_package_t *ptr = (_i2c_package_t *)malloc(sizeof(struct _i2c_package));
	ptr->cmd = 0;
	ptr->decode_stat = 0;
	ptr->index = 0;
	ptr->total = 0;
	ptr->trans_data_total = 0;
	ptr->var_p[0] = NULL;
	ptr->var_p[1] = NULL;
	ptr->var_p[2] = NULL;
	ptr->var_p[3] = NULL;
	return ptr;
}

uint8_t reg_var_4_i2c(void *var_p, uint8_t var_type, _i2c_package_t *ptr){
	if (var_p && ptr){
		if (ptr->total < 4){
	    	ptr->var_p[ptr->total] = var_p;
			ptr->var_type[ptr->total] = var_type;
			ptr->total++;
			return ptr->total;
		}
	}
	return 255;
}
